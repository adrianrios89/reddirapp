import { StackNavigator } from 'react-navigation';
import Posts from '../containers/Posts';
import PostDetails from '../containers/PostDetails';

const navigator = StackNavigator({
  posts: {
    screen: Posts
  },
  postDetails: {
    screen: PostDetails
  }
});

export default navigator;
