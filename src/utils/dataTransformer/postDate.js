import { paddingZero } from '../common';

export const isSameMonthAndYear = (currentDate, postDay) => {
  const isSameMonth = postDay.getMonth() === currentDate.getMonth();
  const isSameYear = currentDate.getFullYear() === postDay.getFullYear();
  return isSameMonth && isSameYear;
};


export const isYesterday = (currentDate, postDay) => {
  const isNextDay = postDay.getDate() === currentDate.getDate() + 1;
  return isNextDay && isSameMonthAndYear(currentDate, postDay);
};

export const isToday = (currentDate, postDay) => {
  const isSameDay = currentDate.getDate() === postDay.getDate();
  return isSameDay && isSameMonthAndYear(currentDate, postDay);
};

export const getDiff = (currentDate, postDay) => {
  const timeDiff = currentDate.getTime() - postDay.getTime();
  const diffDays = Math.trunc(timeDiff / (1000 * 3600 * 24));
  return Math.abs(diffDays);
};

export const getDateWithFormat = (postDay) => {
  return `${paddingZero(postDay.getDate())}/${paddingZero(postDay.getMonth() + 1)}/${postDay.getFullYear()}`;
};

export default postDayTransformer = (timestamp) => {
  const postDay = new Date(timestamp * 1000);
  const currentDate = new Date();
  let dateToReturn;

  if (isToday(currentDate, postDay)) {
    dateToReturn = 'Today';
  } else if (isYesterday(currentDate, postDay)) {
    dateToReturn = 'Tomorrow';
  } else if ((getDiff(currentDate, postDay) >= 0 && (getDiff(currentDate, postDay)) <= 7)) {
    dateToReturn = `${getDiff(currentDate, postDay)} days ago`;
  } else {
    dateToReturn = getDateWithFormat(postDay);
  }
  return dateToReturn;
};
