import { NavigationActions } from 'react-navigation';

export const navigateToRoute = (routeName, params) => {
  const navigateToScreen = NavigationActions.navigate({
    routeName,
    params
  });
  return navigateToScreen;
};
