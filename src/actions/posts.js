import axios from 'axios';
import { ON_NEW_POSTS_REQUEST, ON_NEW_POSTS_SORT_REQUEST, ON_NEW_POSTS_RECEIVED, ON_NEW_POSTS_ERROR, ON_POST_SORT_CHANGE } from '../constants/ActionTypes';
import normalizerPosts from '../normalizers/posts';

export const to = (promise) => {
  return promise.then((data) => {
    return [null, data];
  })
   .catch((err) => {
     return [err];
   });
};

export const onNewPostsRequest = () => {
  return {
    type: ON_NEW_POSTS_REQUEST
  };
};

export const onNewPostsSortRequest = (sort) => {
  return {
    type: ON_NEW_POSTS_SORT_REQUEST,
    sort
  };
};

export const onNewPostsReceived = (response) => {
  return {
    type: ON_NEW_POSTS_RECEIVED,
    response
  };
};

export const onNewError = () => {
  return {
    type: ON_NEW_POSTS_ERROR
  };
};

// export const changePostSort = (sort) => {
//   console.log('CHANGING SORT', sort);
//   return {
//     type: ON_POST_SORT_CHANGE,
//     sort
//   };
// };

export async function getPostsPerSortParam(dispatch, sort) {
  dispatch(onNewPostsSortRequest(sort));

  const [err, response] = await to(axios.get(`https://api.reddit.com/r/pics/${sort.id}`), dispatch);
  if (err) return dispatch(onNewError());
  return dispatch(onNewPostsReceived(normalizerPosts(response)));
}

export const changePostSort = (sort) => {
  return (dispatch) => {
    return getPostsPerSortParam(dispatch, sort);
  };
};


export async function getLastestPostsRequest(dispatch) {
  dispatch(onNewPostsRequest());

  const [err, response] = await to(axios.get('https://api.reddit.com/r/pics/new.json'), dispatch);

  if (err) return dispatch(onNewError());
  return dispatch(onNewPostsReceived(normalizerPosts(response)));
}

export function getLastestPosts() {
  return (dispatch, getState) => {
    console.log('State', getState());
    return getLastestPostsRequest(dispatch);
  };
}
