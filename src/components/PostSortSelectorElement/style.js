export default {
  container: {
    flex: 1,
    margin: 10
  },
  text: {
    textAlign: 'center',
    fontSize: 10,
    color: 'white'
  }
};
