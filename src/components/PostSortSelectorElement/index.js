import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity } from 'react-native';
import style from './style';


class PostSortSelectorElement extends Component {
  render() {
    const isSelected = this.props.sort.id === this.props.id;

    return (
      <TouchableOpacity
        style={style.container}
        onPress={() => {
          this.props.changePostSort({ id: this.props.id, direction: this.props.direction });
        }}
      >
        <Text style={[style.text, { fontWeight: isSelected ? 'bold' : 'normal' }]}>{this.props.name}</Text>
      </TouchableOpacity>
    );
  }
}

PostSortSelectorElement.propTypes = {
  id: PropTypes.string.isRequired,
  direction: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  sort: PropTypes.object.isRequired,
  changePostSort: PropTypes.func.isRequired
};

export default PostSortSelectorElement;
