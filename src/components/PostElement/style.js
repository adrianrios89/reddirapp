export default {
  container: {
    flex: 1,
    margin: 10
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    width: '100%'
  },
  imageSection: {
    mainView: {
      flex: 3,
      alignItems: 'center',
      justifyContent: 'center'
    },
    image: {
      width: 100,
      height: 100
    }
  },
  articleSection: {
    mainView: { flex: 7, padding: 10 },
    dataView: { marginTop: 10 },
    text: { fontSize: 10 },
    dateText: { textAlign: 'right' }
  }
};
