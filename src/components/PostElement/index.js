import React from 'react';
import PropTypes from 'prop-types';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import style from './style';

const PostElement = ({ item, onClick }) => (
  <TouchableOpacity
    style={style.container}
    onPress={() => onClick('postDetails', { id: item.id })}
  >
    <View style={style.content}>
      <View style={style.imageSection.mainView}>
        <Image
          defaultSource={require('../../assets/images/default.png')}
          source={{ uri: item.thumbnail }}
          resizeMode="contain"
          style={style.imageSection.image}
        />
      </View>
      <View style={style.articleSection.mainView}>
        <Text style={[style.articleSection.text, style.articleSection.dateText]}>{item.customDate}</Text>
        <Text>{item.title}</Text>
        <View style={style.articleSection.dataView}>
          <Text style={style.articleSection.text}>{`Author: ${item.author}`}</Text>
          <Text style={style.articleSection.text}>{`Score: ${item.score}`}</Text>
          <Text style={style.articleSection.text}>{`Comments: ${item.numComments}`}</Text>
        </View>
      </View>
    </View>
  </TouchableOpacity>);

PostElement.propTypes = {
  item: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default PostElement;
