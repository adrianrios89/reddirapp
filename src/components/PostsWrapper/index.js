import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FlatList, StatusBar, View } from 'react-native';
import PostElement from '../PostElement';
import PostSortSelectorWrapper from '../PostSortSelectorWrapper';
import style from './style';


class PostsWrapper extends Component {
  componentWillMount() {
    this.props.getLastestPosts();
  }

  render() {
    return (
      <View style={style.container}>
        <StatusBar hidden={true} />
        <PostSortSelectorWrapper
          sort={this.props.sort}
          changePostSort={this.props.changePostSort}
        />
        <FlatList
          style={style.listSection}
          data={this.props.postsList}
          refreshing={this.props.isLoading}
          renderItem={({ item }) => (
            <PostElement item={item} onClick={this.props.navigateToRoute} />
          )}
          onRefresh={this.props.getLastestPosts}
        />
      </View>
    );
  }
}

PostsWrapper.navigationOptions = {
  title: 'Latest Posts'
};

PostsWrapper.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  postsList: PropTypes.array.isRequired,
  sort: PropTypes.object.isRequired,
  getLastestPosts: PropTypes.func.isRequired,
  navigateToRoute: PropTypes.func.isRequired,
  changePostSort: PropTypes.func.isRequired
};

export default PostsWrapper;
