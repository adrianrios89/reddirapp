export default {
  container: {
    flex: 1,
    alignItems: 'center'
  },
  listSection: {
    flex: 1,
    width: '100%'
  }
};
