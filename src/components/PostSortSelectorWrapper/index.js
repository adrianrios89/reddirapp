import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import PostSortSelectorElement from '../PostSortSelectorElement';
import style from './style';

const PostSortSelector = ({ sort, changePostSort }) => (
  <View style={style.container}>
    <PostSortSelectorElement name="New" id="new" direction="asc" sort={sort} changePostSort={changePostSort} />
    <PostSortSelectorElement name="Top" id="top" direction="desc" sort={sort} changePostSort={changePostSort} />
    <PostSortSelectorElement name="Hot" id="hot" direction="desc" sort={sort} changePostSort={changePostSort} />
    <PostSortSelectorElement name="Controversial" direction="desc" id="controversial" sort={sort} changePostSort={changePostSort} />
  </View>
);

PostSortSelector.propTypes = {
  sort: PropTypes.object.isRequired,
  changePostSort: PropTypes.func.isRequired,
};

export default PostSortSelector;
