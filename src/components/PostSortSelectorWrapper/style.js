export default {
  container: {
    height: 30,
    backgroundColor: 'black',
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
};
