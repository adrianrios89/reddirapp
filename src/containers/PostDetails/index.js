import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, WebView } from 'react-native';
import style from './style';

class PostDetails extends Component {
  render() {
    return (
      <View style={style.container}>
        <WebView
          source={{ uri: `https://www.reddit.com/${this.props.navigation.state.params.id}` }}
        />
      </View>
    );
  }
}

PostDetails.navigationOptions = {
  title: 'Post Details',
};

PostDetails.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default PostDetails;
