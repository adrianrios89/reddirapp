import { connect } from 'react-redux';
import { getLastestPosts, changePostSort } from '../actions/posts';
import { navigateToRoute } from '../actions/navigation';
import PostsWrapper from '../components/PostsWrapper';

const mapDispatchToProps = (dispatch) => {
  return ({
    getLastestPosts: () => {
      dispatch(getLastestPosts());
    },
    navigateToRoute: (routeName, params) => {
      dispatch(navigateToRoute(routeName, params));
    },
    changePostSort: (sort) => {
      dispatch(changePostSort(sort));
    },
  });
};

const mapStateToProps = (state) => {
  return ({
    isLoading: state.posts.isLoading,
    postsList: state.posts.postsList,
    sort: state.posts.sort
  });
};

export default connect(mapStateToProps, mapDispatchToProps)(PostsWrapper);
