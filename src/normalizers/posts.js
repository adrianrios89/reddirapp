import transformerDate from '../utils/dataTransformer/postDate';

export default posts = (object) => {
  const posts = object.data.data.children.map((item) => {
    const post = {
      id: item.data.id,
      author: item.data.author,
      createdAt: item.data.created_utc,
      customDate: transformerDate(item.data.created_utc),
      controversial: Math.abs(item.data.ups - item.data.downs),
      key: item.data.id,
      numComments: item.data.num_comments,
      score: item.data.score,
      title: item.data.title,
      top: item.data.ups,
      hot: item.data.ups / item.data.created_utc,
      thumbnail: item.data.thumbnail,
    };
    return post;
  });
  return posts;
};
