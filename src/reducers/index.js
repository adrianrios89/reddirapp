import { combineReducers } from 'redux';
import posts from './posts';
import navigation from './navigation';

const reducer = combineReducers({
  posts,
  navigation
});

export default reducer;
