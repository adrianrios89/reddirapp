import { ON_NEW_POSTS_REQUEST, ON_NEW_POSTS_SORT_REQUEST, ON_NEW_POSTS_RECEIVED, ON_NEW_POSTS_ERROR, ON_POST_SORT_CHANGE } from '../constants/ActionTypes';
import { sortArrays } from '../utils/sort';

const initialState = {
  isLoading: true,
  sort: {
    id: 'new',
    direction: 'asc'
  },
  postsList: []
};


const infoLoader = (state = initialState, action) => {
  switch (action.type) {
    case ON_NEW_POSTS_REQUEST:
      return { ...state, isLoading: true };
    case ON_NEW_POSTS_RECEIVED:
      const newPosts = sortArrays([...action.response], state.sort.id, state.sort.direction);
      return { ...state, postsList: newPosts, isLoading: false };
    case ON_NEW_POSTS_ERROR:
      return { ...state, isLoading: false };
    case ON_NEW_POSTS_SORT_REQUEST:
      return { ...state, sort: action.sort };
    default:
      return state;
  }
};

export default infoLoader;
