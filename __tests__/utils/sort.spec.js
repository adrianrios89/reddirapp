import { sortArrays } from '../../src/utils/sort';

describe('Testing sort alghoritms', () => {
  it('Should sort array with asc direction per property', () => {
    const inputArray = [{ id: 1 }, { id: 4 }, { id: 3 }, { id: 2 }];
    const expectedArray = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];

    expect(sortArrays(inputArray, 'id', 'asc')).toEqual(expectedArray);
  });
  it('Should sort array with desc direction per property', () => {
    const inputArray = [{ id: 1 }, { id: 4 }, { id: 3 }, { id: 2 }];
    const expectedArray = [{ id: 4 }, { id: 3 }, { id: 2 }, { id: 1 }];

    expect(sortArrays(inputArray, 'id', 'desc')).toEqual(expectedArray);
  });
});
