import { isToday, isYesterday, getDiff, isSameMonthAndYear, getDateWithFormat } from '../../src/utils/dataTransformer/postDate';

describe('Testing postDate dataTransformers', () => {
  it('isToday should return true with date is the same day', () => {
    expect(isToday(new Date('2017-02-28T14:14:59Z'), new Date('2017-02-28T20:00:00Z'))).toEqual(true);
    expect(isToday(new Date('1989-04-04T00:00:00Z'), new Date('1989-04-04T16:00:00Z'))).toEqual(true);
    expect(isToday(new Date('1989-04-04T00:00:00Z'), new Date('1989-04-05T16:00:00Z'))).toEqual(false);
  });
  it('isYesterday should return true with date is the previous day', () => {
    expect(isYesterday(new Date('2017-02-27T14:14:59Z'), new Date('2017-02-28T20:00:00Z'))).toEqual(true);
    expect(isYesterday(new Date('1989-04-03T00:00:00Z'), new Date('1989-04-04T16:00:00Z'))).toEqual(true);
    expect(isYesterday(new Date('1989-04-04T00:00:00Z'), new Date('1989-04-04T16:00:00Z'))).toEqual(false);
  });
  it('getDiff should return diff between two dates in days', () => {
    expect(getDiff(new Date('2017-02-28T14:14:59Z'), new Date('2017-02-28T20:00:00Z'))).toEqual(0);
    expect(getDiff(new Date('1989-04-03T00:00:00Z'), new Date('1989-04-04T16:00:00Z'))).toEqual(1);
    expect(getDiff(new Date('1989-04-04T00:00:00Z'), new Date('1989-04-06T16:00:00Z'))).toEqual(2);
  });
  it('isSameMonthAndYear should return true if both dates has the same data', () => {
    expect(isSameMonthAndYear(new Date('2017-02-27T14:14:59Z'), new Date('2017-02-28T20:00:00Z'))).toEqual(true);
    expect(isSameMonthAndYear(new Date('1989-04-03T00:00:00Z'), new Date('1989-04-04T16:00:00Z'))).toEqual(true);
    expect(isSameMonthAndYear(new Date('1989-03-04T00:00:00Z'), new Date('1989-04-04T16:00:00Z'))).toEqual(false);
  });
  it('getDateWithFormat should return date with format DD/MM/YYYY', () => {
    expect(getDateWithFormat(new Date('2017-02-27T14:14:59Z'))).toEqual('27/02/2017');
    expect(getDateWithFormat(new Date('2018-01-15T14:14:59Z'))).toEqual('15/01/2018');
  });
});
