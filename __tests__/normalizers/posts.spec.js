import posts from '../../src/normalizers/posts';

describe('Post Normalizers', () => {
  it('Should normalize posts', () => {
    const objectToBeNormalized = {
      data: {
        data: {
          children: [
            {
              data: {
                id: 1,
                author: 'J.K: Rowling',
                created_utc: 1000000,
                ups: 10,
                downs: 5,
                num_comments: 10,
                score: 10,
                title: 'Harry Potter',
                thumbnail: 'https://awesome-thumbnail'
              }
            }
          ]
        }
      }
    };

    const objectNormalized = [
      {
        author: 'J.K: Rowling',
        controversial: 5,
        createdAt: 1000000,
        customDate: "12/01/1970",
        hot: 0.00001,
        id: 1,
        key: 1,
        numComments: 10,
        score: 10,
        thumbnail: 'https://awesome-thumbnail',
        title: 'Harry Potter',
        top: 10
      }
    ];

    expect(posts(objectToBeNormalized)).toEqual(objectNormalized);
  });
});
