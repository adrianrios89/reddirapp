import React from 'react';
import {
  FlatList,
  StatusBar,
  View
} from 'react-native';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import PostsWrapper from '../../src/components/PostsWrapper';
import PostSortSelectorWrapper from '../../src/components/PostSortSelectorWrapper';

Enzyme.configure({ adapter: new Adapter() });

describe('<PostsWrapper /> Component', () => {
  it('PostsWrapper should match with the snapshot', () => {
    const tree = renderer.create(
      <PostsWrapper isLoading={false} postsList={[]} sort={{}} getLastestPosts={() => {}} navigateToRoute={() => {}} changePostSort={() => {}} />
    );
    const json = tree.toJSON();
    expect(json).toMatchSnapshot();
  });

  const postsWrapper = shallow(<PostsWrapper isLoading={false} postsList={[]} sort={{}} getLastestPosts={() => {}} navigateToRoute={() => {}} changePostSort={() => {}} />);

  it('PostsWrapper should exists', () => {
    expect(postsWrapper.length).toEqual(1);
  });

  it('PostsWrapper should has one View', () => {
    expect(postsWrapper.find(View)).toHaveLength(1);
  });

  it('PostsWrapper should has one StatusBar', () => {
    expect(postsWrapper.find(StatusBar)).toHaveLength(1);
  });

  it('PostsWrapper should has one PostSortSelectorWrapper', () => {
    expect(postsWrapper.find(PostSortSelectorWrapper)).toHaveLength(1);
  });

  it('PostsWrapper should has one FlatList', () => {
    expect(postsWrapper.find(FlatList)).toHaveLength(1);
  });
});
