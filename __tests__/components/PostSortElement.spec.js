import React from 'react';
import {
  Text,
  TouchableOpacity
} from 'react-native';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import PostSortSelectoElement from '../../src/components/PostSortSelectorElement';

Enzyme.configure({ adapter: new Adapter() });

describe('<PostSortSelectoElement /> Component', () => {
  it('PostSortSelectoElement should match with the snapshot', () => {
    const tree = renderer.create(
      <PostSortSelectoElement name="New" id="createdAt" direction="asc" sort={{ id: 'test', direction: 'test' }} changePostSort={() => {}} />
    );
    const json = tree.toJSON();
    expect(json).toMatchSnapshot();
  });

  const postSortSelectorElementShallow = shallow(<PostSortSelectoElement name="New" id="createdAt" direction="asc" sort={{ id: 'test', direction: 'test' }} changePostSort={() => {}} />);

  it('PostSortSelectoElement should exists', () => {
    expect(postSortSelectorElementShallow.length).toEqual(1);
  });

  it('PostSortSelectoElement should has one TouchableOpacity', () => {
    expect(postSortSelectorElementShallow.find(TouchableOpacity)).toHaveLength(1);
  });

  it('PostSortSelectoElement should has one Text', () => {
    expect(postSortSelectorElementShallow.find(Text)).toHaveLength(1);
  });
});
