import React from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import PostSortSelectorWrapper from '../../src/components/PostSortSelectorWrapper';
import PostSortSelectorElement from '../../src/components/PostSortSelectorElement';

Enzyme.configure({ adapter: new Adapter() });

describe('<PostSortSelectorWrapper /> Component', () => {
  it('PostSortSelectorWrapper should match with the snapshot', () => {
    const tree = renderer.create(
      <PostSortSelectorWrapper sort={{ id: 'test', direction: 'test' }} changePostSort={() => {}} />
    );
    const json = tree.toJSON();
    expect(json).toMatchSnapshot();
  });

  const postSortSelectorWrapper = shallow(<PostSortSelectorWrapper sort={{ id: 'test', direction: 'test' }} changePostSort={() => {}} />);

  it('PostSortSelectorWrapper should exists', () => {
    expect(postSortSelectorWrapper.length).toEqual(1);
  });

  it('PostSortSelectorWrapper should has one View', () => {
    expect(postSortSelectorWrapper.find(View)).toHaveLength(1);
  });

  it('PostSortSelectorElement should has four PostSortSelectorElement', () => {
    expect(postSortSelectorWrapper.find(PostSortSelectorElement)).toHaveLength(4);
  });
});
