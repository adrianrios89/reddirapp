import React from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
import PostElement from '../../src/components/PostElement';

Enzyme.configure({ adapter: new Adapter() });

describe('<PostElement /> Component', () => {
  it('PostElement should match with the snapshot', () => {
    const tree = renderer.create(
      <PostElement item={{}} onClick={() => {}} />
    );
    const json = tree.toJSON();
    expect(json).toMatchSnapshot();
  });

  const postElement = shallow(<PostElement item={{}} onClick={() => {}} />);

  it('PostElement should exists', () => {
    expect(postElement.length).toEqual(1);
  });

  it('PostElement should has one TouchableOpacity', () => {
    expect(postElement.find(TouchableOpacity)).toHaveLength(1);
  });

  it('PostElement should has five Text', () => {
    expect(postElement.find(Text)).toHaveLength(5);
  });

  it('PostElement should has four View', () => {
    expect(postElement.find(View)).toHaveLength(4);
  });
});
