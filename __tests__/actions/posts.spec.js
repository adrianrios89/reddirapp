import nock from 'nock';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import iso from 'isomorphic-fetch';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import * as actions from '../../src/actions/posts';
import { ON_NEW_POSTS_REQUEST, ON_NEW_POSTS_ERROR, ON_NEW_POSTS_RECEIVED, ON_POST_SORT_CHANGE } from '../../src/constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Post actions', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('should dispatch ON_NEW_POSTS_REQUEST when onNewPostsRequest is fired', () => {
    const expectedActions = [
      { type: ON_NEW_POSTS_REQUEST }
    ];
    const store = mockStore();
    store.dispatch(actions.onNewPostsRequest());
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch ON_NEW_POSTS_ERROR when onNewError is fired', () => {
    const expectedActions = [
      { type: ON_NEW_POSTS_ERROR }
    ];
    const store = mockStore();
    store.dispatch(actions.onNewError());
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch ON_NEW_POSTS_RECEIVED when onNewPostsReceived is fired', () => {
    const response = [{ id: 1, title: 'Dragon Ball rocks' }];
    const expectedActions = [
      { type: ON_NEW_POSTS_RECEIVED, response }
    ];
    const store = mockStore();
    store.dispatch(actions.onNewPostsReceived(response));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should dispatch ON_POST_SORT_CHANGE when changePostSort is fired', () => {
    const sort = { id: 'createdAt', direction: 'asc' };
    const expectedActions = [
      { type: ON_POST_SORT_CHANGE, sort }
    ];
    const store = mockStore();
    store.dispatch(actions.changePostSort(sort));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('Should dispatch ON_NEW_POSTS_REQUEST, ON_NEW_POSTS_RECEIVED ON_NEW_POSTS_REQUEST when getLastestPostsRequest fired and request works ok', () => {
    const store = mockStore();
    const response = {
      data: {
        children: [
          {
            data: {
              id: 1,
              author: 'J.K: Rowling',
              created_utc: 1000000,
              ups: 10,
              downs: 5,
              num_comments: 10,
              score: 10,
              title: 'Harry Potter',
              thumbnail: 'https://awesome-thumbnail'
            }
          }
        ]
      }
    };

    const responseNormalized =
      [
        {
          author: 'J.K: Rowling',
          controversial: 5,
          createdAt: 1000000,
          customDate: "12/01/1970",
          hot: 0.00001,
          id: 1,
          key: 1,
          numComments: 10,
          score: 10,
          thumbnail: 'https://awesome-thumbnail',
          title: 'Harry Potter',
          top: 10
        }
      ];

    const mock = new MockAdapter(axios);
    mock.onGet('https://api.reddit.com/r/pics/new.json').reply(200, response);

    const expectedActions = [
        { type: ON_NEW_POSTS_REQUEST },
        { type: ON_NEW_POSTS_RECEIVED, response: responseNormalized }
    ];

    return actions.getLastestPostsRequest(store.dispatch).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('Should dispatch ON_NEW_POSTS_REQUEST, ON_NEW_POSTS_RECEIVED ON_NEW_POSTS_REQUEST when getLastestPostsRequest fired and request works ok', () => {
    const store = mockStore();

    const mock = new MockAdapter(axios);
    mock.onGet('https://api.reddit.com/r/pics/new.json').reply(500);

    const expectedActions = [
        { type: ON_NEW_POSTS_REQUEST },
        { type: ON_NEW_POSTS_ERROR }
    ];

    return actions.getLastestPostsRequest(store.dispatch).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
