import account from '../../src/reducers/posts';
import { ON_NEW_POSTS_REQUEST, ON_NEW_POSTS_RECEIVED, ON_NEW_POSTS_ERROR, ON_POST_SORT_CHANGE } from '../../src/constants/ActionTypes';

const initialState = {
  isLoading: true,
  sort: {
    id: 'createdAt',
    direction: 'asc'
  },
  postsList: []
};


describe('Posts reducer', () => {
  it('should return default state', () => {
    expect(account(undefined, {})).toEqual(initialState);
  });

  it('Should show loading when ON_NEW_POSTS_REQUEST action fired', () => {
    expect(account(initialState, { type: ON_NEW_POSTS_REQUEST })).toEqual({ ...initialState, isLoading: true });
  });

  it('Should update post and hide loading when ON_NEW_POSTS_RECEIVED action fired', () => {
    expect(account(initialState, { type: ON_NEW_POSTS_RECEIVED, response: [{ id: 1 }] })).toEqual({ ...initialState, isLoading: false, postsList: [{ id: 1 }] });
  });

  it('Should  hide loading when ON_NEW_POSTS_ERROR action fired', () => {
    expect(account(initialState, { type: ON_NEW_POSTS_ERROR })).toEqual({ ...initialState, isLoading: false });
  });

  it('Should sort when ON_POST_SORT_CHANGE action fired', () => {
    const state = { ...initialState, postsList: [{ id: 1, createdAt: 10 }, { id: 2, createdAt: 5 }] };
    expect(account(state, { type: ON_POST_SORT_CHANGE, sort: { id: 'createdAt', direction: 'asc' } })).toEqual({ ...initialState, postsList: [{ id: 2, createdAt: 5 }, { id: 1, createdAt: 10 }] });
  });
});
