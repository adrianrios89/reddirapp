# Reddit APP
A React-Native + Redux + React Navigation mobile app to check the latest posts on Reddit.

![rn](https://img.shields.io/badge/React%20Native--blue.svg)
![ios](https://img.shields.io/badge/IOS--blue.svg)
![android](https://img.shields.io/badge/Android--blue.svg)
![redux-navigation](https://img.shields.io/badge/react%20navigation--yellowgreen.svg)
![redux](https://img.shields.io/badge/Redux--yellowgreen.svg)
![jest](https://img.shields.io/badge/Jest%20--green.svg)

![Imgur](https://i.imgur.com/FNNvtfI.png)
![Imgur](https://i.imgur.com/8syxHYF.png)
## Setup & Run


### 1. Install React Native

https://facebook.github.io/react-native/docs/getting-started.html


### 2. Install dependencies:

Navigate to the repo's directory.

Run:

```npm install```

### 3. Run the application:

Run:

```react-native run-ios``` or ```react-native run-android``` 

### 4. Run the tests:

Run:

```npm run test```







